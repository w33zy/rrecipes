# == Schema Information
#
# Table name: recipes
#
#  id          :bigint           not null, primary key
#  cook_time   :time
#  description :text(65535)
#  notes       :text(65535)
#  prep_time   :time
#  title       :string(255)
#  total_time  :time
#  yield       :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class RecipeSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :prep_time, :cook_time, :total_time, :notes

  has_many :ingredient_lines, key: :ingredients
  has_many :instructions

  def prep_time
      object.prep_time.strftime('%H:%M:%S') if object.prep_time
  end

  def cook_time
    object.cook_time.strftime('%H:%M:%S') if object.cook_time
  end

  def total_time
    object.total_time.strftime('%H:%M:%S') if object.total_time
  end
end
