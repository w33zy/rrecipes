# == Schema Information
#
# Table name: ingredient_lines
#
#  id         :bigint           not null, primary key
#  amount     :string(255)
#  link       :string(255)
#  note       :string(255)
#  sort       :integer
#  unit       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  recipe_id  :bigint           not null
#
# Indexes
#
#  index_ingredient_lines_on_recipe_id  (recipe_id)
#
# Foreign Keys
#
#  fk_rails_...  (recipe_id => recipes.id)
#
class IngredientLineSerializer < ActiveModel::Serializer
  attributes :id, :sort, :amount, :unit, :ingredient, :note, :link

  belongs_to :recipe

  def ingredient
    object.ingredients.first.attributes.delete_if {|k| k == 'created_at' || k == 'updated_at'}
  end
end
