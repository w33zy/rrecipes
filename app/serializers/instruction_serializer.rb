# == Schema Information
#
# Table name: instructions
#
#  id         :bigint           not null, primary key
#  details    :text(65535)
#  image      :string(255)
#  link       :string(255)
#  sort       :integer
#  video      :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  recipe_id  :bigint           not null
#
# Indexes
#
#  index_instructions_on_recipe_id  (recipe_id)
#
# Foreign Keys
#
#  fk_rails_...  (recipe_id => recipes.id)
#
class InstructionSerializer < ActiveModel::Serializer
  attributes :id, :sort, :details, :image, :link, :video

  belongs_to :recipe
end
