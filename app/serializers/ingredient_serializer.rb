# == Schema Information
#
# Table name: ingredients
#
#  id         :bigint           not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class IngredientSerializer < ActiveModel::Serializer
  attributes :id, :name

  has_many :ingredient_lines
end
