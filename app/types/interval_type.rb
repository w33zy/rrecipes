class IntervalType < ActiveRecord::Type::Value
  def cast_value(value)
    return super if value.instance_of? Time

    Time.at(ActiveSupport::Duration.parse(value).to_i).utc
  end

=begin
  def serialize(value)
    ActiveSupport::Duration === value ? value.iso8601 : value
  end
=end
end
