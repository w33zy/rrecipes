class Api::V1::RecipesController < ApplicationController
  before_action :set_recipe, only: [:show, :update, :destroy]

  def index
    render json: Recipe.all
  end

  def show
    render json: @recipe
  end

  def create
    @recipe = Recipe.new(recipe_params)

    if @recipe.save

      render json: @recipe, status: :created
    else
      render json: @recipe.errors, status: :unprocessable_entity
    end
  end

  def update
    if @recipe.update(recipe_params)
      render json: @recipe
    else
      render json: @recipe.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @recipe.destroy
      render json: {recipe_removed: @recipe.id}, status: :ok
    else
      render json: {invalid_recipe: params[:id]}, status: :not_found
    end
  end

  private

    def set_recipe
      @recipe = Recipe.find(params[:id])
    end

    def recipe_params
      params.require(:recipe).permit(
        :title,
        :description,
        :notes,
        :prep_time,
        :cook_time,
        :total_time,
        instructions_attributes: [:id, :sort, :details, :image, :video, :link, :_destroy],
        ingredient_lines_attributes: [:id, :sort, :amount, :unit, :note, :link, :_destroy, ingredients_attributes: [:id, :name]],
      )
    end
end
