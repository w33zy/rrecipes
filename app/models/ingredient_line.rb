# == Schema Information
#
# Table name: ingredient_lines
#
#  id         :bigint           not null, primary key
#  amount     :string(255)
#  link       :string(255)
#  note       :string(255)
#  sort       :integer
#  unit       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  recipe_id  :bigint           not null
#
# Indexes
#
#  index_ingredient_lines_on_recipe_id  (recipe_id)
#
# Foreign Keys
#
#  fk_rails_...  (recipe_id => recipes.id)
#
class IngredientLine < ApplicationRecord
  belongs_to :recipe
  has_and_belongs_to_many :ingredients

  accepts_nested_attributes_for :ingredients

  def ingredients_attributes=(attributes)
    attributes.values_at('name').each do |ingredient_value|
      ingredient = Ingredient.find_or_create_by(name: ingredient_value)
      self.ingredients << ingredient
    end
  end
end
