# == Schema Information
#
# Table name: recipes
#
#  id          :bigint           not null, primary key
#  cook_time   :time
#  description :text(65535)
#  notes       :text(65535)
#  prep_time   :time
#  title       :string(255)
#  total_time  :time
#  yield       :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Recipe < ApplicationRecord
  attribute :prep_time, :interval
  attribute :cook_time, :interval
  attribute :total_time, :interval

  has_many :ingredient_lines, dependent: :destroy
  has_many :instructions, dependent: :destroy

  accepts_nested_attributes_for :ingredient_lines, allow_destroy: true
  accepts_nested_attributes_for :instructions, allow_destroy: true
end
