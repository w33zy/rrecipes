class CreateJoinTableIngredientIngredientLine < ActiveRecord::Migration[6.0]
  def change
    create_join_table :ingredients, :ingredient_lines do |t|
      # t.index [:ingredient_id, :ingredient_line_id]
      # t.index [:ingredient_line_id, :ingredient_id]
    end
  end
end
