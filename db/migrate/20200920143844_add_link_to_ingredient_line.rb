class AddLinkToIngredientLine < ActiveRecord::Migration[6.0]
  def change
    add_column :ingredient_lines, :link, :string
  end
end
