class CreateIngredientLines < ActiveRecord::Migration[6.0]
  def change
    create_table :ingredient_lines do |t|
      t.string :sort
      t.string :amount
      t.string :unit
      t.string :note
      t.references :recipe, null: false, foreign_key: true

      t.timestamps
    end
  end
end
