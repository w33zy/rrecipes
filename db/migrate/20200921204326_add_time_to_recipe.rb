class AddTimeToRecipe < ActiveRecord::Migration[6.0]
  def change
    add_column :recipes, :prep_time, :time
    add_column :recipes, :cook_time, :time
    add_column :recipes, :total_time, :time
    add_column :recipes, :yield, :string
  end
end
