class ChangeIngredientLineSort < ActiveRecord::Migration[6.0]
  def change
    change_column :ingredient_lines, :sort, :integer
  end
end
